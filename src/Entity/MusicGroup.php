<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\MusicGroupRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=MusicGroupRepository::class)
 */
class MusicGroup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"music-group"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"music-group"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"music-group"})
     */
    private $image;

    /**
     * @ORM\Column(type="text")
     * @Groups({"music-group"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"music-group"})
     */
    private $playerAudio;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPlayerAudio(): ?string
    {
        return $this->playerAudio;
    }

    public function setPlayerAudio(?string $playerAudio): self
    {
        $this->playerAudio = $playerAudio;

        return $this;
    }
}
