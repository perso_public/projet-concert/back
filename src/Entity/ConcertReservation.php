<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Repository\ConcertReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(normalizationContext={"groups"={"concert-reservation", "ref-value", "user"}}, denormalizationContext={"groups"={"concert-reservation", "concert-ticket-reservation"}})
 * @ORM\Entity(repositoryClass=ConcertReservationRepository::class)
 */
class ConcertReservation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("concert-reservation")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-reservation")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-reservation")
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-reservation")
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-reservation")
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-reservation")
     */
    private $line1;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-reservation")
     */
    private $building;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("concert-reservation")
     */
    private $namedPlace;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-reservation")
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-reservation")
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-reservation")
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-reservation")
     */
    private $phone;

    /**
     * @ORM\Column(type="date")
     * @Groups("concert-reservation")
     */
    private $birthday;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("concert-reservation")
     */
    private $purchaseDate;

    /**
     * @ORM\OneToMany(targetEntity=ConcertTicketReservation::class, mappedBy="concertReservation", cascade={"persist"})
     * @Groups("concert-reservation")
     */
    private $concertTicketsReservation;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-reservation")
     */
    private $reservationNumber;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=true)
     * @Groups("concert-reservation")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups("concert-reservation")
     */
    private $updateDate;

    public function __construct()
    {
        $this->concertTicketsReservation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLine1(): ?string
    {
        return $this->line1;
    }

    public function setLine1(string $line1): self
    {
        $this->line1 = $line1;

        return $this;
    }

    public function getBuilding(): ?string
    {
        return $this->building;
    }

    public function setBuilding(string $building): self
    {
        $this->building = $building;

        return $this;
    }

    public function getNamedPlace(): ?string
    {
        return $this->namedPlace;
    }

    public function setNamedPlace(?string $namedPlace): self
    {
        $this->namedPlace = $namedPlace;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getPurchaseDate(): ?\DateTimeInterface
    {
        return $this->purchaseDate;
    }

    public function setPurchaseDate(\DateTimeInterface $purchaseDate): self
    {
        $this->purchaseDate = $purchaseDate;

        return $this;
    }

    /**
     * @return Collection|ConcertTicketReservation[]
     */
    public function getConcertTicketsReservation(): Collection
    {
        return $this->concertTicketsReservation;
    }

    public function addConcertTicketsReservation(ConcertTicketReservation $concertTicketsReservation): self
    {
        if (!$this->concertTicketsReservation->contains($concertTicketsReservation)) {
            $this->concertTicketsReservation[] = $concertTicketsReservation;
            $concertTicketsReservation->setConcertReservation($this);
        }

        return $this;
    }

    public function removeConcertTicketsReservation(ConcertTicketReservation $concertTicketsReservation): self
    {
        if ($this->concertTicketsReservation->removeElement($concertTicketsReservation)) {
            // set the owning side to null (unless already changed)
            if ($concertTicketsReservation->getConcertReservation() === $this) {
                $concertTicketsReservation->setConcertReservation(null);
            }
        }

        return $this;
    }

    public function getReservationNumber(): ?string
    {
        return $this->reservationNumber;
    }

    public function setReservationNumber(string $reservationNumber): self
    {
        $this->reservationNumber = $reservationNumber;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }
}
