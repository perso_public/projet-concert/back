<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ConcertRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(normalizationContext={"groups"={"concert", "concert-ticket", "music-group", "app-file", "ref-value", "concert-hall", "address", "location"}}, denormalizationContext={"groups"={"concert", "music-group", "concert-ticket"}})    
 * @ApiFilter(SearchFilter::class, properties={
 *          "musicCategory.value": "exact",
 *          "concertHall.location.name": "exact",
 * })
 * @ApiFilter(DateFilter::class, properties={"date"})
 * @ApiFilter(OrderFilter::class, properties={"date", "musicGroup.name"})
 * @ORM\Entity(repositoryClass=ConcertRepository::class)
 */
class Concert
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("concert")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert")
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity=MusicGroup::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @ApiSubresource
     * @Groups("concert")
     */
    private $musicGroup;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("concert")
     */
    private $date;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("concert")
     */
    private $openingHours;

    /**
     * @ORM\ManyToOne(targetEntity=RefValue::class)
     * @ORM\JoinColumn(nullable=true)
     * @ApiSubresource
     * @Groups("concert")
     */
    private $musicCategory;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("concert")
     */
    private $isDeleted;

    /**
     * @ORM\ManyToOne(targetEntity=ConcertHall::class)
     * @ORM\JoinColumn(nullable=true)
     * @ApiSubresource
     * @Groups("concert")
     */
    private $concertHall;

    /**
     * @ORM\OneToMany(targetEntity=ConcertTicket::class, mappedBy="concert", cascade={"persist"})
     * @ApiSubresource
     * @Groups("concert")
     */
    private $concertTickets;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups("concert")
     */
    private $minPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups("concert")
     */
    private $maxPrice;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups("concert")
     */
    private $decreasingPercentage;

    public function __construct()
    {
        $this->concertTickets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getMusicGroup(): ?MusicGroup
    {
        return $this->musicGroup;
    }

    public function setMusicGroup(?MusicGroup $musicGroup): self
    {
        $this->musicGroup = $musicGroup;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getOpeningHours(): ?\DateTimeInterface
    {
        return $this->openingHours;
    }

    public function setOpeningHours(\DateTimeInterface $openingHours): self
    {
        $this->openingHours = $openingHours;

        return $this;
    }

    public function getMusicCategory(): ?RefValue
    {
        return $this->musicCategory;
    }

    public function setMusicCategory(?RefValue $musicCategory): self
    {
        $this->musicCategory = $musicCategory;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getConcertHall(): ?ConcertHall
    {
        return $this->concertHall;
    }

    public function setConcertHall(?ConcertHall $concertHall): self
    {
        $this->concertHall = $concertHall;

        return $this;
    }

    /**
     * @return Collection|ConcertTicket[]
     */
    public function getConcertTickets(): Collection
    {
        return $this->concertTickets;
    }

    public function addConcertTicket(ConcertTicket $concertTicket): self
    {
        if (!$this->concertTickets->contains($concertTicket)) {
            $this->concertTickets[] = $concertTicket;
            $concertTicket->setConcert($this);
        }

        return $this;
    }

    public function removeConcertTicket(ConcertTicket $concertTicket): self
    {
        if ($this->concertTickets->removeElement($concertTicket)) {
            // set the owning side to null (unless already changed)
            if ($concertTicket->getConcert() === $this) {
                $concertTicket->setConcert(null);
            }
        }

        return $this;
    }

    public function getDecreasingPercentage(): ?int
    {
        return $this->decreasingPercentage;
    }

    public function setDecreasingPercentage(int $decreasingPercentage): self
    {
        $this->decreasingPercentage = $decreasingPercentage;

        return $this;
    }

    public function getMaxPrice(): ?float
    {
        return $this->maxPrice;
    }

    public function setMaxPrice(float $maxPrice): self
    {
        $this->maxPrice = $maxPrice;

        return $this;
    }

    public function getMinPrice(): ?float
    {
        return $this->minPrice;
    }

    public function setMinPrice(float $minPrice): self
    {
        $this->minPrice = $minPrice;

        return $this;
    }
}
