<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\AddressRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=AddressRepository::class)
 */
class Address
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("address")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("address")
     */
    private $building;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("address")
     */
    private $namedPlace;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("address")
     * @Assert\NotBlank(message="Un code postal est obligatoire")
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("address")
     * @Assert\NotBlank(message="Une ville est obligatoire")
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("address")
     * @Assert\NotBlank(message="Un pays est obligatoire")
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("address")
     * @Assert\NotBlank(message="Une adresse est obligatoire")
     */
    private $line1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("address")
     */
    private $line2;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBuilding(): ?string
    {
        return $this->building;
    }

    public function setBuilding(?string $building): self
    {
        $this->building = $building;

        return $this;
    }

    public function getNamedPlace(): ?string
    {
        return $this->namedPlace;
    }

    public function setNamedPlace(?string $namedPlace): self
    {
        $this->namedPlace = $namedPlace;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getLine1(): ?string
    {
        return $this->line1;
    }

    public function setLine1(string $line1): self
    {
        $this->line1 = $line1;

        return $this;
    }

    public function getLine2(): ?string
    {
        return $this->line2;
    }

    public function setLine2(?string $line2): self
    {
        $this->line2 = $line2;

        return $this;
    }
}
