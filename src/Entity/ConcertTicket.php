<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ConcertTicketRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(normalizationContext={"groups"={"concert-ticket", "concert-ticket-concert", "music-group", "ref-value", "concert", "app-file", "concert-hall", "address", "location"}})    
 * @ORM\Entity(repositoryClass=ConcertTicketRepository::class)
 */
class ConcertTicket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("concert-ticket")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=RefValue::class)
     * @ORM\JoinColumn(nullable=false)
     * @ApiSubresource
     * @Groups("concert-ticket")
     */
    private $category;

    /**
     * @ORM\Column(type="float")
     * @Groups("concert-ticket")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Concert::class, inversedBy="concertTickets")
     * @ORM\JoinColumn(nullable=false)
     * @ApiSubresource
     * @Groups("concert-ticket-concert")
     */
    private $concert;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategory(): ?RefValue
    {
        return $this->category;
    }

    public function setCategory(?RefValue $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getConcert(): ?Concert
    {
        return $this->concert;
    }

    public function setConcert(?Concert $concert): self
    {
        $this->concert = $concert;

        return $this;
    }
}
