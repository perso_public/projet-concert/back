<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\ConcertTicketReservationRepository;

/**
 * @ApiResource(normalizationContext={"groups"={"concert-ticket-reservation", "concert-ticket", "ref-value", "concert-reservation"}}, denormalizationContext={"groups"={"concert-ticket-reservation"}})
 * @ORM\Entity(repositoryClass=ConcertTicketReservationRepository::class)
 */
class ConcertTicketReservation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("concert-ticket-reservation")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=ConcertTicket::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups("concert-ticket-reservation")
     * @ApiSubresource
     */
    private $concertTicket;

    /**
     * @ORM\ManyToOne(targetEntity=RefValue::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups("concert-ticket-reservation")
     * @ApiSubresource
     */
    private $obtainingMethod;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("concert-ticket-reservation")
     */
    private $insurance;

    /**
     * @ORM\Column(type="integer")
     * @Groups("concert-ticket-reservation")
     */
    private $spanNumber;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-ticket-reservation")
     */
    private $rowNumber;

    /**
     * @ORM\ManyToOne(targetEntity=ConcertReservation::class, inversedBy="concertTicketsReservation")
     * @Groups("concert-ticket-reservation")
     * @ApiSubresource
     */
    private $concertReservation;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups("concert-ticket-reservation")
     */
    private $price;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConcertTicket(): ?ConcertTicket
    {
        return $this->concertTicket;
    }

    public function setConcertTicket(ConcertTicket $concertTicket): self
    {
        $this->concertTicket = $concertTicket;

        return $this;
    }

    public function getObtainingMethod(): ?RefValue
    {
        return $this->obtainingMethod;
    }

    public function setObtainingMethod(?RefValue $obtainingMethod): self
    {
        $this->obtainingMethod = $obtainingMethod;

        return $this;
    }

    public function getInsurance(): ?bool
    {
        return $this->insurance;
    }

    public function setInsurance(bool $insurance): self
    {
        $this->insurance = $insurance;

        return $this;
    }

    public function getSpanNumber(): ?int
    {
        return $this->spanNumber;
    }

    public function setSpanNumber(int $spanNumber): self
    {
        $this->spanNumber = $spanNumber;

        return $this;
    }

    public function getRowNumber(): ?string
    {
        return $this->rowNumber;
    }

    public function setRowNumber(string $rowNumber): self
    {
        $this->rowNumber = $rowNumber;

        return $this;
    }

    public function getConcertReservation(): ?ConcertReservation
    {
        return $this->concertReservation;
    }

    public function setConcertReservation(?ConcertReservation $concertReservation): self
    {
        $this->concertReservation = $concertReservation;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
}
