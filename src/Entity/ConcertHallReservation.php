<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\ConcertHallReservationRepository;

/**
 * @ApiResource(normalizationContext={"groups"={"concert-hall-reservation", "concert-hall", "ref-value", "user", "app-file", "address", "location"}})
 * @ORM\Entity(repositoryClass=ConcertHallReservationRepository::class)
 */
class ConcertHallReservation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("concert-hall-reservation")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-hall-reservation")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-hall-reservation")
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-hall-reservation")
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("concert-hall-reservation")
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-hall-reservation")
     */
    private $phone;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("concert-hall-reservation")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     * @Groups("concert-hall-reservation")
     */
    private $peopleNumber;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("concert-hall-reservation")
     */
    private $budget;

    /**
     * @ORM\Column(type="text")
     * @Groups("concert-hall-reservation")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=ConcertHall::class)
     * @ORM\JoinColumn(nullable=false)
     * @ApiSubresource
     * @Groups("concert-hall-reservation")
     */
    private $concertHall;

    /**
     * @ORM\ManyToOne(targetEntity=RefValue::class)
     * @ORM\JoinColumn(nullable=false)
     * @ApiSubresource
     * @Groups("concert-hall-reservation")
     */
    private $state;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups("concert-hall-reservation")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPeopleNumber(): ?int
    {
        return $this->peopleNumber;
    }

    public function setPeopleNumber(int $peopleNumber): self
    {
        $this->peopleNumber = $peopleNumber;

        return $this;
    }

    public function getBudget(): ?string
    {
        return $this->budget;
    }

    public function setBudget(string $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getConcertHall(): ?ConcertHall
    {
        return $this->concertHall;
    }

    public function setConcertHall(?ConcertHall $concertHall): self
    {
        $this->concertHall = $concertHall;

        return $this;
    }

    public function getState(): ?RefValue
    {
        return $this->state;
    }

    public function setState(?RefValue $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
