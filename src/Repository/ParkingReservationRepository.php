<?php

namespace App\Repository;

use App\Entity\ParkingReservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ParkingReservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParkingReservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParkingReservation[]    findAll()
 * @method ParkingReservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParkingReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParkingReservation::class);
    }

    // /**
    //  * @return ParkingReservation[] Returns an array of ParkingReservation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ParkingReservation
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
