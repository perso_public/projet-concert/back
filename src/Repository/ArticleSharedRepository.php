<?php

namespace App\Repository;

use App\Entity\ArticleShared;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ArticleShared|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticleShared|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticleShared[]    findAll()
 * @method ArticleShared[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleSharedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArticleShared::class);
    }

    // /**
    //  * @return ArticleShared[] Returns an array of ArticleShared objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ArticleShared
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
