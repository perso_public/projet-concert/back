<?php

namespace App\Repository;

use App\Entity\ConcertTicketReservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConcertTicketReservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConcertTicketReservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConcertTicketReservation[]    findAll()
 * @method ConcertTicketReservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConcertTicketReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConcertTicketReservation::class);
    }

    // /**
    //  * @return ConcertTicketReservation[] Returns an array of ConcertTicketReservation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConcertTicketReservation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
