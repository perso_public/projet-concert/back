<?php

namespace App\Repository;

use App\Entity\RefValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RefValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method RefValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method RefValue[]    findAll()
 * @method RefValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RefValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RefValue::class);
    }

    // /**
    //  * @return RefValue[] Returns an array of RefValue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RefValue
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
