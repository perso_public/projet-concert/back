<?php

namespace App\Repository;

use App\Entity\ConcertHallReservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConcertHallReservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConcertHallReservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConcertHallReservation[]    findAll()
 * @method ConcertHallReservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConcertHallReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConcertHallReservation::class);
    }

    // /**
    //  * @return ConcertHallReservation[] Returns an array of ConcertHallReservation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConcertHallReservation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
