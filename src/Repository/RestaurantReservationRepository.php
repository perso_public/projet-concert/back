<?php

namespace App\Repository;

use App\Entity\RestaurantReservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RestaurantReservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method RestaurantReservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method RestaurantReservation[]    findAll()
 * @method RestaurantReservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RestaurantReservation::class);
    }

    // /**
    //  * @return RestaurantReservation[] Returns an array of RestaurantReservation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RestaurantReservation
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
