<?php

namespace App\Repository;

use App\Entity\ConcertReservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConcertReservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConcertReservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConcertReservation[]    findAll()
 * @method ConcertReservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConcertReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConcertReservation::class);
    }

    // /**
    //  * @return ConcertReservation[] Returns an array of ConcertReservation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConcertReservation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
