<?php

namespace App\Repository;

use App\Entity\AppFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AppFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppFile[]    findAll()
 * @method AppFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AppFile::class);
    }

    // /**
    //  * @return AppFile[] Returns an array of AppFile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AppFile
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
