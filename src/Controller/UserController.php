<?php
namespace App\Controller;

// header("Access-Control-Allow-Origin: *");

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\User;
use App\Entity\Address;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use App\Repository\AddressRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserController extends AbstractController
{
    /**
     * @Route("/api/user/registration", name="registration", methods={"post"})
     */
    public function registration(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, ValidatorInterface $validator, UserPasswordEncoderInterface $encoder, UserRepository $userRepository): Response
    {
        $var =  $request->getContent();
        $error = "";

        try {
            $userInformation = $serializer->decode($var, 'json');
            $password = $userInformation['password'];

            $address = new Address();
            $address->setLine1($userInformation['line1']);
            $address->setBuilding($userInformation['building']);
            $address->setNamedPlace($userInformation['namedPlace']);
            $address->setPostalCode($userInformation['postalCode']);
            $address->setCity($userInformation['city']);
            $address->setCountry($userInformation['country']);

            $user = new User();
            if ($userInformation['email'] != $userInformation['emailConfirmation']){
                $error .= ' Votre email n\'est pas le même.';
            }
            if ($password != $userInformation['passwordConfirmation']){
                $error .= ' Votre mot de passe n\'est pas le même.';
            }
            if ($error != ""){
                return $this->json([
                    'status' => 404,
                    'error' => $error
                ]);
            }

            $user->setEmail($userInformation['email']);
            $user->setPassword($userInformation['password']);
            $user->setGender($userInformation['gender']);
            $user->setLastName($userInformation['lastName']);
            $user->setFirstName($userInformation['firstName']);
            $user->setPhone($userInformation['phone']);
            $user->setBirthdate(new \DateTime($userInformation['birthdate']));
            $user->setRegisterDate(new \DateTime());
            $user->setAddress($address);

            $validator->validate($user);
            $validator->validate($address);

            $encoded = $encoder->encodePassword($user, $password);
            $user->setPassword($encoded);

            if(isset($errors)) {
                return $this->json($errors, 400);
            }

            $em->persist($address);
            $em->persist($user);
            $em->flush();

            $userFind = $userRepository->findOneBy(['email' => $user->getEmail()]);

            return $this->json($userFind, 200);
        } catch(NotEncodableValueException $e){
            return $this->json([
                'status' => 400,
                'error' => $e->geterror()
            ]);
        }
    }

    /**
     * @Route("/api/user/connection", name="connection", methods={"post"})
     */
    public function connection(UserRepository $userRepository, Request $request, SerializerInterface $serializer, UserPasswordEncoderInterface $encoder): Response
    {
        $var =  $request->getContent();
        $user = $serializer->decode($var, 'json');
        $userFind = $userRepository->findOneBy(['email' => $user['email']]);
        $encoded = $encoder->isPasswordValid($userFind, $user['password']);

        if ($userFind == NULL){
            return $this->json([
                'status' => 400,
                'error' => 'Mot de passe erroné'
            ]);
        }

        if($encoded == false){
            return $this->json([
                'status' => 400,
                'error' => 'Mot de passe erroné'
            ]);
        }

        return $this->json($userFind, 200);
    }

    /**
     * @Route("/api/user/update", name="update", methods={"put"})
     */
    public function update(UserRepository $userRepository, AddressRepository $addressRepository, Request $request, SerializerInterface $serializer, EntityManagerInterface $em): Response
    {
        $var =  $request->getContent();
        $user = $serializer->decode($var, 'json');
        $userFind = $userRepository->findOneBy(['id' => $user['id']]);
        $articleId = ($userFind->getAddress()->getId());
        $addressFind = $addressRepository->findOneBy(['id' => $articleId]);

        if ($userFind == NULL){
            return $this->json([
                'status' => 400,
                'error' => 'Un problème est survenu veuillez réessayer'
            ]);
        }
        $userFind->setFirstName($user['firstName']);
        $userFind->setLastName($user['lastName']);
        $userFind->setEmail($user['email']);
        $userFind->setPhone($user['phone']);
        $userFind->setGender($user['gender']);
        $userFind->setBirthdate(new \DateTime($user['birthdate']));

        $addressFind->setLine1($user['line1']);
        $addressFind->setCity($user['city']);
        $addressFind->setCountry($user['country']);
        $addressFind->setPostalCode($user['postalCode']);
        $addressFind->setNamedPlace($user['namedPlace']);
        $addressFind->setBuilding($user['building']);

        $em->flush();

        return $this->json($userFind, 200);
    }
}