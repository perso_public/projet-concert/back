<?php

namespace App\DataFixtures;

use App\Entity\ConcertTicket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ConcertTicketFixtures extends Fixture implements DependentFixtureInterface
{
    public const ACDC_CAT1_TICKET_REFERENCE = 'acdc-cat1-ticket';
    public const ACDC_CAT2_TICKET_REFERENCE = 'acdc-cat2-ticket';
    public const ACDC_CAT3_TICKET_REFERENCE = 'acdc-cat3-ticket';

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $ticketAcdc1 = new ConcertTicket();
        $ticketAcdc1->setCategory($this->getReference(RefValueFixtures::CAT1_TICKET_REFERENCE))
                    ->setPrice(800)
                    ->setConcert($this->getReference(ConcertFixtures::ACDC1_CONCERT_REFERENCE));

        $manager->persist($ticketAcdc1);

        $ticketAcdc2 = new ConcertTicket();
        $ticketAcdc2->setCategory($this->getReference(RefValueFixtures::CAT2_TICKET_REFERENCE))
                    ->setPrice(700)
                    ->setConcert($this->getReference(ConcertFixtures::ACDC1_CONCERT_REFERENCE));

        $manager->persist($ticketAcdc2);

        $ticketAcdc3 = new ConcertTicket();
        $ticketAcdc3->setCategory($this->getReference(RefValueFixtures::CAT3_TICKET_REFERENCE))
                    ->setPrice(600)
                    ->setConcert($this->getReference(ConcertFixtures::ACDC1_CONCERT_REFERENCE));

        $manager->persist($ticketAcdc3);
        $manager->flush();

        $this->addReference(self::ACDC_CAT1_TICKET_REFERENCE, $ticketAcdc1);
        $this->addReference(self::ACDC_CAT2_TICKET_REFERENCE, $ticketAcdc2);
        $this->addReference(self::ACDC_CAT3_TICKET_REFERENCE, $ticketAcdc3);


        $ticket2Acdc1 = new ConcertTicket();
        $ticket2Acdc1->setCategory($this->getReference(RefValueFixtures::CAT1_TICKET_REFERENCE))
                    ->setPrice(800)
                    ->setConcert($this->getReference(ConcertFixtures::ACDC2_CONCERT_REFERENCE));
        $manager->persist($ticket2Acdc1);

        $ticket2Acdc2 = new ConcertTicket();
        $ticket2Acdc2->setCategory($this->getReference(RefValueFixtures::CAT2_TICKET_REFERENCE))
                    ->setPrice(700)
                    ->setConcert($this->getReference(ConcertFixtures::ACDC2_CONCERT_REFERENCE));
        $manager->persist($ticket2Acdc2);

        $ticket2Acdc3 = new ConcertTicket();
        $ticket2Acdc3->setCategory($this->getReference(RefValueFixtures::CAT3_TICKET_REFERENCE))
                    ->setPrice(600)
                    ->setConcert($this->getReference(ConcertFixtures::ACDC2_CONCERT_REFERENCE));
        $manager->persist($ticket2Acdc3);


        $ticket2Jul1 = new ConcertTicket();
        $ticket2Jul1->setCategory($this->getReference(RefValueFixtures::CAT1_TICKET_REFERENCE))
                    ->setPrice(800)
                    ->setConcert($this->getReference(ConcertFixtures::JUL_CONCERT_REFERENCE));
        $manager->persist($ticket2Jul1);

        $ticket2Jul2 = new ConcertTicket();
        $ticket2Jul2->setCategory($this->getReference(RefValueFixtures::CAT2_TICKET_REFERENCE))
                    ->setPrice(700)
                    ->setConcert($this->getReference(ConcertFixtures::JUL_CONCERT_REFERENCE));
        $manager->persist($ticket2Jul2);

        $ticket2Jul3 = new ConcertTicket();
        $ticket2Jul3->setCategory($this->getReference(RefValueFixtures::CAT3_TICKET_REFERENCE))
                    ->setPrice(600)
                    ->setConcert($this->getReference(ConcertFixtures::JUL_CONCERT_REFERENCE));
        $manager->persist($ticket2Jul3);


        $ticket1Ter1 = new ConcertTicket();
        $ticket1Ter1->setCategory($this->getReference(RefValueFixtures::CAT1_TICKET_REFERENCE))
                    ->setPrice(90)
                    ->setConcert($this->getReference(ConcertFixtures::TER1_CONCERT_REFERENCE));
        $manager->persist($ticket1Ter1);

        $ticket1Ter2 = new ConcertTicket();
        $ticket1Ter2->setCategory($this->getReference(RefValueFixtures::CAT2_TICKET_REFERENCE))
                    ->setPrice(70)
                    ->setConcert($this->getReference(ConcertFixtures::TER1_CONCERT_REFERENCE));
        $manager->persist($ticket1Ter2);

        $ticket1Ter3 = new ConcertTicket();
        $ticket1Ter3->setCategory($this->getReference(RefValueFixtures::CAT3_TICKET_REFERENCE))
                    ->setPrice(50)
                    ->setConcert($this->getReference(ConcertFixtures::TER1_CONCERT_REFERENCE));
        $manager->persist($ticket1Ter3);

        $ticket2Ter1 = new ConcertTicket();
        $ticket2Ter1->setCategory($this->getReference(RefValueFixtures::CAT1_TICKET_REFERENCE))
                    ->setPrice(90)
                    ->setConcert($this->getReference(ConcertFixtures::TER2_CONCERT_REFERENCE));
        $manager->persist($ticket2Ter1);

        $ticket2Ter2 = new ConcertTicket();
        $ticket2Ter2->setCategory($this->getReference(RefValueFixtures::CAT2_TICKET_REFERENCE))
                    ->setPrice(70)
                    ->setConcert($this->getReference(ConcertFixtures::TER2_CONCERT_REFERENCE));
        $manager->persist($ticket2Ter2);

        $ticket2Ter3 = new ConcertTicket();
        $ticket2Ter3->setCategory($this->getReference(RefValueFixtures::CAT3_TICKET_REFERENCE))
                    ->setPrice(50)
                    ->setConcert($this->getReference(ConcertFixtures::TER2_CONCERT_REFERENCE));
        $manager->persist($ticket2Ter3);
        
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            MusicGroupFixtures::class,
            RefValueFixtures::class,
            ConcertFixtures::class,
        );
    }
}
