<?php

namespace App\DataFixtures;

use App\Entity\MusicGroup;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class MusicGroupFixtures extends Fixture implements DependentFixtureInterface
{
    public const ACDC_GROUP_REFERENCE = 'acdc-group';
    public const JUL_GROUP_REFERENCE = 'jul-group';
    public const TER_GROUP_REFERENCE = 'ter-group';

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $acdc = new MusicGroup();
        $acdc->setName('AC/DC');
        $acdc->setImage('https://www.nme.com/wp-content/uploads/2020/10/ACDC-the-band.jpg');
        $acdc->setDescription("AC/DC est un groupe de hard rock australo-britannique, originaire de Sydney. Il est formé en 1973 par les frères écossais Angus et Malcolm Young. Bien que classé dans le hard rock et considéré comme un pionnier de ce genre musical, ainsi que parfois du heavy metal2,3, les membres du groupe ont toujours qualifié leur musique de « rock 'n' roll »4. Elle pourrait aussi être catégorisée comme hard blues. La composition d'AC/DC subit beaucoup de changements avant la sortie du deuxième album en Australie, T.N.T., en 1975. Elle est ensuite restée inchangée jusqu'à ce que Mark Evans soit remplacé par Cliff Williams en 1977. En 1979, le groupe connaît un succès mondial avec son tube Highway to Hell. Le chanteur et co-parolier Bon Scott meurt le 19 février 1980 d'une asphyxie causée par ses propres vomissements, après une nuit de forte consommation d'alcool. Le groupe envisage alors la dissolution mais l'ancien chanteur de Geordie, Brian Johnson, est choisi comme remplaçant de Bon Scott. La même année, le groupe sort l'album Back in Black, leur plus grand succès mondial. L'album suivant en 1981, For Those About to Rock We Salute You, a aussi beaucoup de succès et est le premier de leurs albums à atteindre la première place des ventes aux États-Unis.");
        $acdc->setPlayerAudio('https://www.youtube.com/watch?v=v2AC41dglnM');
        $manager->persist($acdc);

        $jul = new MusicGroup();
        $jul->setName('JUL');
        $jul->setImage('https://img.lemde.fr/2020/11/18/494/0/8688/4340/1440/720/60/0/7d2c869_856070765-jul-fifou-0888.jpg');
        $jul->setDescription("Jul, de son vrai nom Julien Mari1, est un rappeur et chanteur français, né le 14 janvier 1990 dans le 12e arrondissement de Marseille. Il publie son premier single, Sort le cross volé, en novembre 2013 suivi en février 2014 d'un album entier, Dans ma paranoïa, le premier d'une série prolifique : deux albums complets par an depuis le début de sa carrière, tous certifiés au moins disque de platine. En 2015, Jul quitte le label Liga One Industry à la suite de désaccords financiers et fonde son propre label indépendant, D'or et de platine. L'année suivante, il reçoit la récompense du meilleur album de musique urbaine aux 32es Victoires de la musique pour l'album My World. En février 2020, il devient le plus gros vendeur de disque de l'histoire du rap français avec plus de 4 millions d'albums vendus à l'âge de 30 ans, et en six ans de carrière2.");
        $jul->setPlayerAudio('https://www.youtube.com/watch?v=qWpBduQKTMY');
        $manager->persist($jul);

        $ter = new MusicGroup();
        $ter->setName('47ter');
        $ter->setImage('https://www.tendanceouest.com/photos/maxi/338889.jpg');
        $ter->setDescription("47Ter est un groupe musical composé de Pierre Delamotte dit 'Pierre-Paul', Blaise Ligouzat et Miguel Lopes (« Lopes »)1. Ils sont amis depuis le collège, et sont originaires de Bailly, dans les Yvelines. Le groupe s'est formé en 2017. Ils ont débuté par des vidéos sur YouTube. Ils se font alors connaître grâce à leurs vidéos 'on vient gâcher tes classiques', où ils reprenaient des thèmes de chanson célèbres et chantaient par-dessus. Le 47Ter a une forte inspiration pop-rock, et n'hésite pas à le montrer, voire même à en être fier : leur On vient gâcher tes classiques n°4 reprenant un morceau du groupe AC-DC en est un bon exemple. De plus, ils ont collaboré avec des artistes de variété française tels que Patrick Bruel qui apparait dans le clip Côte Ouest, et ont également fait une reprise de la Corrida de Francis Cabrel.");
        $ter->setPlayerAudio('https://www.youtube.com/watch?v=uQUmEai2JhA');
        $manager->persist($ter);


        $manager->flush();

        $this->addReference(self::ACDC_GROUP_REFERENCE, $acdc);
        $this->addReference(self::JUL_GROUP_REFERENCE, $jul);
        $this->addReference(self::TER_GROUP_REFERENCE, $ter);
    }

    public function getDependencies()
    {
        return array(
            AppFileFixtures::class,
        );
    }
}
