<?php

namespace App\DataFixtures;

use App\Entity\ConcertHall;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ConcertHallFixtures extends Fixture implements DependentFixtureInterface
{
    public const AIX_CONCERT_HALL_REFERENCE = 'aix-concert-hall';
    public const BOURGES_CONCERT_HALL_REFERENCE = 'bourges-concert-hall';
    public const CANNES_CONCERT_HALL_REFERENCE = 'cannes-concert-hall';
    public const DUNKERQUE_CONCERT_HALL_REFERENCE = 'dunkerque-concert-hall';
    public const ECHIROLLES_CONCERT_HALL_REFERENCE = 'echirolles-concert-hall';

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $aixConcertHall = new ConcertHall();
        $aixConcertHall->setName('Le Grand Théâtre');
        $aixConcertHall->setImage('https://awayandfar.com/wp-content/uploads/2019/10/Cours-Mirabeau.jpg');
        $aixConcertHall->setAddress($this->getReference(AddressFixtures::AIX_ADDRESS_REFERENCE));
        $aixConcertHall->setLocation($this->getReference(LocationFixtures::AIX_LOCATION_REFERENCE));
        $manager->persist($aixConcertHall);

        $bourgesConcertHall = new ConcertHall();
        $bourgesConcertHall->setName('Les Rives d\'Auron');
        $bourgesConcertHall->setImage('https://www.ville-bourges.fr/__medias__/images/culture-loisirs/palais-auron.jpg');
        $bourgesConcertHall->setAddress($this->getReference(AddressFixtures::BOURGES_ADDRESS_REFERENCE));
        $bourgesConcertHall->setLocation($this->getReference(LocationFixtures::BOURGES_LOCATION_REFERENCE));
        $manager->persist($bourgesConcertHall);

        $cannesConcertHall = new ConcertHall();
        $cannesConcertHall->setName('Palais des Festivals et des Congrès de Cannes');
        $cannesConcertHall->setImage('https://www.deplacementspros.com/wp-content/uploads/2020/08/palais-cannes-1920px-de-large-72-dpi-1.jpg');
        $cannesConcertHall->setAddress($this->getReference(AddressFixtures::CANNES_ADDRESS_REFERENCE));
        $cannesConcertHall->setLocation($this->getReference(LocationFixtures::CANNES_LOCATION_REFERENCE));
        $manager->persist($cannesConcertHall);

        $dunkerqueConcertHall = new ConcertHall();
        $dunkerqueConcertHall->setName('Les 4 Ecluses');
        $dunkerqueConcertHall->setImage('https://4ecluses.com/sites/default/files/les4ecluses/styles/16x9_1920/public/ged/le-lieu-ambianceimg_9903.jpg?itok=3m4PGrvq');
        $dunkerqueConcertHall->setAddress($this->getReference(AddressFixtures::DUNKERQUE_ADDRESS_REFERENCE));
        $dunkerqueConcertHall->setLocation($this->getReference(LocationFixtures::DUNKERQUE_LOCATION_REFERENCE));
        $manager->persist($dunkerqueConcertHall);

        $echirollesConcertHall = new ConcertHall();
        $echirollesConcertHall->setName('La Rampe');
        $echirollesConcertHall->setImage('https://www.echirolles.fr/sites/default/files/2017-03/La_Rampe_1.jpg');
        $echirollesConcertHall->setAddress($this->getReference(AddressFixtures::ECHIROLLES_ADDRESS_REFERENCE));
        $echirollesConcertHall->setLocation($this->getReference(LocationFixtures::ECHIROLLES_LOCATION_REFERENCE));
        $manager->persist($echirollesConcertHall);

        $manager->flush();

        $this->addReference(self::AIX_CONCERT_HALL_REFERENCE, $aixConcertHall);
        $this->addReference(self::BOURGES_CONCERT_HALL_REFERENCE, $bourgesConcertHall);
        $this->addReference(self::CANNES_CONCERT_HALL_REFERENCE, $cannesConcertHall);
        $this->addReference(self::DUNKERQUE_CONCERT_HALL_REFERENCE, $dunkerqueConcertHall);
        $this->addReference(self::ECHIROLLES_CONCERT_HALL_REFERENCE, $echirollesConcertHall);
    }

    public function getDependencies()
    {
        return array(
            LocationFixtures::class,
            AddressFixtures::class,
            AppFileFixtures::class,
        );
    }
}
