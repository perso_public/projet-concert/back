<?php

namespace App\DataFixtures;

use App\Entity\Address;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AddressFixtures extends Fixture
{
    public const AIX_ADDRESS_REFERENCE = 'aix-address';
    public const BOURGES_ADDRESS_REFERENCE = 'bourges-address';
    public const CANNES_ADDRESS_REFERENCE = 'cannes-address';
    public const DUNKERQUE_ADDRESS_REFERENCE = 'dunkerque-address';
    public const ECHIROLLES_ADDRESS_REFERENCE = 'echirolles-address';

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        
        $aixAddress = new Address();
        $aixAddress->setPostalCode('13100');
        $aixAddress->setCity('Aix-en-Provence');
        $aixAddress->setCountry('France');
        $aixAddress->setLine1('380 Avenue Max Juvénal');
        $manager->persist($aixAddress);

        $bourgesAddress = new Address();
        $bourgesAddress->setPostalCode('18000');
        $bourgesAddress->setCity('Bourges');
        $bourgesAddress->setCountry('France');
        $bourgesAddress->setLine1('7 Boulevard Lamarck');
        $manager->persist($bourgesAddress);

        $cannesAddress = new Address();
        $cannesAddress->setPostalCode('06400');
        $cannesAddress->setCity('Cannes');
        $cannesAddress->setCountry('France');
        $cannesAddress->setLine1('1 Boulevard de la Croisette');
        $manager->persist($cannesAddress);

        $dunkerqueAddress = new Address();
        $dunkerqueAddress->setPostalCode('59140');
        $dunkerqueAddress->setCity('Dunkerque');
        $dunkerqueAddress->setCountry('France');
        $dunkerqueAddress->setLine1('Rue de la Cunette');
        $manager->persist($dunkerqueAddress);

        $echirollesAddress = new Address();
        $echirollesAddress->setPostalCode('38130');
        $echirollesAddress->setCity('Echirolles');
        $echirollesAddress->setCountry('France');
        $echirollesAddress->setLine1('15 Avenue du 8 Mai 1945');
        $manager->persist($echirollesAddress);

        $manager->flush();

        $this->addReference(self::AIX_ADDRESS_REFERENCE, $aixAddress);
        $this->addReference(self::BOURGES_ADDRESS_REFERENCE, $bourgesAddress);
        $this->addReference(self::CANNES_ADDRESS_REFERENCE, $cannesAddress);
        $this->addReference(self::DUNKERQUE_ADDRESS_REFERENCE, $dunkerqueAddress);
        $this->addReference(self::ECHIROLLES_ADDRESS_REFERENCE, $echirollesAddress);
    }
}
