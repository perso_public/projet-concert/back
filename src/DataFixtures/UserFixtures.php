<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Role;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $userRole = new Role();
        $userRole->setName("ROLE_USER");
        $manager->persist($userRole);

        $adminRole = new Role();
        $adminRole->setName("ROLE_ADMIN");
        $manager->persist($adminRole);

        $address = new Address();
        $address->setLine1('12 immeuble giga grand la bas');
        $address->setBuilding('');
        $address->setNamedPlace('');
        $address->setPostalCode('12');
        $address->setCity('New York');
        $address->setCountry('Etats-Unis');
        $manager->persist($address);

        $address2 = new Address();
        $address2->setLine1('12 Rue des 5 hamsters');
        $address2->setBuilding('');
        $address2->setNamedPlace('');
        $address2->setPostalCode('13500');
        $address2->setCity('Aix en Provence');
        $address2->setCountry('France');
        $manager->persist($address2);

        $manager->flush();

        $user = new User();
        $user->setEmail('user@mail.fr')
            ->setPassword($this->encoder->encodePassword($user, 'user'))
            ->addRole($userRole)
            ->setFirstName('user')
            ->setLastName('user')
            ->setPhone('0212458934')
            ->setGender('man')
            ->setRegisterDate(new DateTime('now'))
            ->setBirthdate(new DateTime('2000-01-01'))
            ->setAddress($address);
        $manager->persist($user);

        $user = new User();
        $user->setEmail('admin@mail.fr')
            ->setPassword($this->encoder->encodePassword($user, 'admin'))
            ->addRole($userRole)
            ->addRole($adminRole)
            ->setFirstName('admin')
            ->setLastName('admin')
            ->setPhone('0212458934')
            ->setGender('woman')
            ->setRegisterDate(new DateTime('now'))
            ->setBirthdate(new DateTime('2000-02-02'))
            ->setAddress($address2);

        $manager->persist($user);

        $manager->flush();
    }
}
