<?php

namespace App\DataFixtures;

use App\Entity\Location;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class LocationFixtures extends Fixture
{
    public const AIX_LOCATION_REFERENCE = 'aix-location';
    public const BOURGES_LOCATION_REFERENCE = 'bourges-location';
    public const CANNES_LOCATION_REFERENCE = 'cannes-location';
    public const DUNKERQUE_LOCATION_REFERENCE = 'dunkerque-location';
    public const ECHIROLLES_LOCATION_REFERENCE = 'echirolles-location';


    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $aix = new Location();
        $aix->setName("Aix-en-Provence");
        $manager->persist($aix);

        $bourges = new Location();
        $bourges->setName("Bourges");
        $manager->persist($bourges);

        $cannes = new Location();
        $cannes->setName("Cannes");
        $manager->persist($cannes);

        $dunkerque = new Location();
        $dunkerque->setName("Dunkerque");
        $manager->persist($dunkerque);

        $echirolles = new Location();
        $echirolles->setName("Echirolles");
        $manager->persist($echirolles);

        $manager->flush();


        $this->addReference(self::AIX_LOCATION_REFERENCE, $aix);
        $this->addReference(self::BOURGES_LOCATION_REFERENCE, $bourges);
        $this->addReference(self::CANNES_LOCATION_REFERENCE, $cannes);
        $this->addReference(self::DUNKERQUE_LOCATION_REFERENCE, $dunkerque);
        $this->addReference(self::ECHIROLLES_LOCATION_REFERENCE, $echirolles);
    }
}
