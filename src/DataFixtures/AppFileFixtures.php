<?php

namespace App\DataFixtures;

use App\Entity\AppFile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFileFixtures extends Fixture
{
    public const AIX_FILE_REFERENCE = 'aix-file';
    public const BOURGES_FILE_REFERENCE = 'bourges-file';
    public const CANNES_FILE_REFERENCE = 'cannes-file';
    public const DUNKERQUE_FILE_REFERENCE = 'dunkerque-file';
    public const ECHIROLLES_FILE_REFERENCE = 'echirolles-file';

    public const ACDC_FILE_REFERENCE = 'acdc-file';
    public const JUL_FILE_REFERENCE = 'jul-file';
    public const TER_FILE_REFERENCE = 'ter-file';

    public const ACDC_CONCERT_FILE_REFERENCE = 'acdc-concert-file';
    public const JUL_CONCERT_FILE_REFERENCE = 'jul-concert-file';

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        //Photos des salles de concert
        $aixImage = new AppFile();
        $aixImage->setFilename('salle_aix.jpeg');
        $manager->persist($aixImage);

        $bourgesImage = new AppFile();
        $bourgesImage->setFilename('salle_bourges.jpeg');
        $manager->persist($bourgesImage);

        $cannesImage = new AppFile();
        $cannesImage->setFilename('salle_cannes.png');
        $manager->persist($cannesImage);

        $dunkerqueImage = new AppFile();
        $dunkerqueImage->setFilename('salle_dunkerque.png');
        $manager->persist($dunkerqueImage);

        $echirollesImage = new AppFile();
        $echirollesImage->setFilename('salle_echirolles.png');
        $manager->persist($echirollesImage);

        $manager->flush();

        $this->addReference(self::AIX_FILE_REFERENCE, $aixImage);
        $this->addReference(self::BOURGES_FILE_REFERENCE, $bourgesImage);
        $this->addReference(self::CANNES_FILE_REFERENCE, $cannesImage);
        $this->addReference(self::DUNKERQUE_FILE_REFERENCE, $dunkerqueImage);
        $this->addReference(self::ECHIROLLES_FILE_REFERENCE, $echirollesImage);


        //Photos des groupes de musique
        $acdcImage = new AppFile();
        $acdcImage->setFilename('acdc.jpeg');
        $manager->persist($acdcImage);

        $julImage = new AppFile();
        $julImage->setFilename('jul.jpeg');
        $manager->persist($julImage);

        $terImage = new AppFile();
        $terImage->setFilename('47ter.png');
        $manager->persist($terImage);

        $manager->flush();

        $this->addReference(self::ACDC_FILE_REFERENCE, $acdcImage);
        $this->addReference(self::JUL_FILE_REFERENCE, $julImage);
        $this->addReference(self::TER_FILE_REFERENCE, $terImage);


        //Photos des concerts
        $acdcConcertImage = new AppFile();
        $acdcConcertImage->setFilename('acdc_concert.jpeg');
        $manager->persist($acdcConcertImage);

        $julConcertImage = new AppFile();
        $julConcertImage->setFilename('jul_concert.jpeg');
        $manager->persist($julConcertImage);

        $manager->flush();

        $this->addReference(self::ACDC_CONCERT_FILE_REFERENCE, $acdcConcertImage);
        $this->addReference(self::JUL_CONCERT_FILE_REFERENCE, $julConcertImage);
    }
}
