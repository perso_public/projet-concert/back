<?php

namespace App\DataFixtures;

use App\Entity\Concert;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ConcertFixtures extends Fixture implements DependentFixtureInterface
{
    public const ACDC1_CONCERT_REFERENCE = 'acdc1-concert';
    public const ACDC2_CONCERT_REFERENCE = 'acdc2-concert';
    public const JUL_CONCERT_REFERENCE = 'jul-concert';
    public const TER1_CONCERT_REFERENCE = 'ter1-concert';
    public const TER2_CONCERT_REFERENCE = 'ter2-concert';
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $acdcConcert1 = new Concert();
        $acdcConcert1->setName('Rock or Bust World Tour')
                    ->setImage('https://www.nme.com/wp-content/uploads/2020/10/ACDC-the-band.jpg')
                    ->setMusicGroup($this->getReference(MusicGroupFixtures::ACDC_GROUP_REFERENCE))
                    ->setDate(new \DateTime('2021-05-22 20:00'))
                    ->setOpeningHours(new \DateTime('2021-05-22 18:32'))
                    ->setMusicCategory($this->getReference(RefValueFixtures::ROCK_MUSIC_REFERENCE))
                    ->setIsDeleted(false)
                    ->setConcertHall($this->getReference(ConcertHallFixtures::AIX_CONCERT_HALL_REFERENCE))
                    ->setDecreasingPercentage(5)
                    ->setMaxPrice(1000)
                    ->setMinPrice(600);
        $manager->persist($acdcConcert1);

        $acdcConcert2 = new Concert();
        $acdcConcert2->setName('Rock or Bust World Tour')
                    ->setImage('https://www.nme.com/wp-content/uploads/2020/10/ACDC-the-band.jpg')
                    ->setMusicGroup($this->getReference(MusicGroupFixtures::ACDC_GROUP_REFERENCE))
                    ->setDate(new \DateTime('2021-05-30 20:00'))
                    ->setOpeningHours(new \DateTime('2021-05-30 18:32'))
                    ->setMusicCategory($this->getReference(RefValueFixtures::ROCK_MUSIC_REFERENCE))
                    ->setIsDeleted(false)
                    ->setConcertHall($this->getReference(ConcertHallFixtures::DUNKERQUE_CONCERT_HALL_REFERENCE))
                    ->setDecreasingPercentage(5)
                    ->setMaxPrice(1000)
                    ->setMinPrice(600);
        $manager->persist($acdcConcert2);

        $julConcert1 = new Concert();
        $julConcert1->setName('C\'est pas des LOL')
                    ->setImage('https://img.lemde.fr/2020/11/18/494/0/8688/4340/1440/720/60/0/7d2c869_856070765-jul-fifou-0888.jpg')
                    ->setMusicGroup($this->getReference(MusicGroupFixtures::JUL_GROUP_REFERENCE))
                    ->setDate(new \DateTime('2021-07-14 19:00'))
                    ->setOpeningHours(new \DateTime('2021-07-14 17:50'))
                    ->setMusicCategory($this->getReference(RefValueFixtures::RAP_MUSIC_REFERENCE))
                    ->setIsDeleted(false)
                    ->setConcertHall($this->getReference(ConcertHallFixtures::AIX_CONCERT_HALL_REFERENCE))
                    ->setDecreasingPercentage(5)
                    ->setMaxPrice(1000)
                    ->setMinPrice(600);

        $manager->persist($julConcert1);

        $terConcert1 = new Concert();
        $terConcert1->setName('47TOUR')
                    ->setImage('https://www.fnacspectacles.com/static/0/visuel/310/429/47TER-TOUR_4297087678870546068.jpg')
                    ->setMusicGroup($this->getReference(MusicGroupFixtures::TER_GROUP_REFERENCE))
                    ->setDate(new \DateTime('2021-08-23 21:00'))
                    ->setOpeningHours(new \DateTime('2021-08-23 18:50'))
                    ->setMusicCategory($this->getReference(RefValueFixtures::RAP_MUSIC_REFERENCE))
                    ->setIsDeleted(false)
                    ->setConcertHall($this->getReference(ConcertHallFixtures::AIX_CONCERT_HALL_REFERENCE))
                    ->setDecreasingPercentage(5)
                    ->setMaxPrice(100)
                    ->setMinPrice(50);
        $manager->persist($terConcert1);

        $terConcert2 = new Concert();
        $terConcert2->setName('47TOUR')
                    ->setImage('https://www.fnacspectacles.com/static/0/visuel/310/429/47TER-TOUR_4297087678870546068.jpg')
                    ->setMusicGroup($this->getReference(MusicGroupFixtures::TER_GROUP_REFERENCE))
                    ->setDate(new \DateTime('2021-08-24 21:00'))
                    ->setOpeningHours(new \DateTime('2021-08-24 18:50'))
                    ->setMusicCategory($this->getReference(RefValueFixtures::RAP_MUSIC_REFERENCE))
                    ->setIsDeleted(false)
                    ->setConcertHall($this->getReference(ConcertHallFixtures::CANNES_CONCERT_HALL_REFERENCE))
                    ->setDecreasingPercentage(5)
                    ->setMaxPrice(100)
                    ->setMinPrice(50);
        $manager->persist($terConcert2);
    
        $manager->flush();

        $this->addReference(self::ACDC1_CONCERT_REFERENCE, $acdcConcert1);
        $this->addReference(self::ACDC2_CONCERT_REFERENCE, $acdcConcert2);
        $this->addReference(self::JUL_CONCERT_REFERENCE, $julConcert1);
        $this->addReference(self::TER1_CONCERT_REFERENCE, $terConcert1);
        $this->addReference(self::TER2_CONCERT_REFERENCE, $terConcert2);
    }
    
    public function getDependencies()
    {
        return array(
            AppFileFixtures::class,
            MusicGroupFixtures::class,
            RefValueFixtures::class,
            ConcertHallFixtures::class,
        );
    }
}
