<?php

namespace App\DataFixtures;

use App\Entity\RefValue;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class RefValueFixtures extends Fixture
{
    public const POP_MUSIC_REFERENCE = 'pop-music';
    public const ROCK_MUSIC_REFERENCE = 'rock-music';
    public const ELECTRO_MUSIC_REFERENCE = 'electro-music';
    public const RAP_MUSIC_REFERENCE = 'rap-music';
    public const SOUL_MUSIC_REFERENCE = 'soul-music';
    public const CLASSIQUE_MUSIC_REFERENCE = 'classique-music';
    public const DUB_MUSIC_REFERENCE = 'dub-music';
    public const WORLD_MUSIC_REFERENCE = 'world-music';

    public const CAT1_TICKET_REFERENCE = 'cat1-ticket';
    public const CAT2_TICKET_REFERENCE = 'cat2-ticket';
    public const CAT3_TICKET_REFERENCE = 'cat3-ticket';

    public const OBTAINING_METHOD1_REFERENCE = 'guichet';
    public const OBTAINING_METHOD2_REFERENCE = 'e-ticket';
    public const OBTAINING_METHOD3_REFERENCE = 'envoi';


    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        //Types de musiques
        $musicType = 'MusicCategory';

        $pop = new RefValue();
        $pop->setCode($musicType . 'Pop');
        $pop->setTypeCode($musicType . 'Code');
        $pop->setValue('Pop');
        $manager->persist($pop);

        $rock = new RefValue();
        $rock->setCode($musicType . 'Rock');
        $rock->setTypeCode($musicType . 'Code');
        $rock->setValue('Rock');
        $manager->persist($rock);

        $electro = new RefValue();
        $electro->setCode($musicType . 'Electro');
        $electro->setTypeCode($musicType . 'Code');
        $electro->setValue('Electro');
        $manager->persist($electro);

        $rap = new RefValue();
        $rap->setCode($musicType . 'Rap');
        $rap->setTypeCode($musicType . 'Code');
        $rap->setValue('Rap / Hip-Hop');
        $manager->persist($rap);

        $soul = new RefValue();
        $soul->setCode($musicType . 'Soul');
        $soul->setTypeCode($musicType . 'Code');
        $soul->setValue('Soul / Funk');
        $manager->persist($soul);

        $classique = new RefValue();
        $classique->setCode($musicType . 'Classique');
        $classique->setTypeCode($musicType . 'Code');
        $classique->setValue('Classique');
        $manager->persist($classique);

        $dub = new RefValue();
        $dub->setCode($musicType . 'Dub');
        $dub->setTypeCode($musicType . 'Code');
        $dub->setValue('Dub / Raggae');
        $manager->persist($dub);

        $world = new RefValue();
        $world->setCode($musicType . 'World');
        $world->setTypeCode($musicType . 'Code');
        $world->setValue('World');
        $manager->persist($world);

        $manager->flush();

        $this->addReference(self::POP_MUSIC_REFERENCE, $pop);
        $this->addReference(self::ROCK_MUSIC_REFERENCE, $rock);
        $this->addReference(self::ELECTRO_MUSIC_REFERENCE, $electro);
        $this->addReference(self::RAP_MUSIC_REFERENCE, $rap);
        $this->addReference(self::SOUL_MUSIC_REFERENCE, $soul);
        $this->addReference(self::CLASSIQUE_MUSIC_REFERENCE, $classique);
        $this->addReference(self::DUB_MUSIC_REFERENCE, $dub);
        $this->addReference(self::WORLD_MUSIC_REFERENCE, $world);


        //Types de tickets de concert
        $concertTicket = 'ConcertTicketCategory';

        $cat1 = new RefValue();
        $cat1->setCode($concertTicket . 'Cat1');
        $cat1->setTypeCode($concertTicket . 'Code');
        $cat1->setValue('Cat1');
        $manager->persist($cat1);

        $cat2 = new RefValue();
        $cat2->setCode($concertTicket . 'Cat2');
        $cat2->setTypeCode($concertTicket . 'Code');
        $cat2->setValue('Cat2');
        $manager->persist($cat2);

        $cat3 = new RefValue();
        $cat3->setCode($concertTicket . 'Cat3');
        $cat3->setTypeCode($concertTicket . 'Code');
        $cat3->setValue('Cat3');
        $manager->persist($cat3);

        $manager->flush();

        $this->addReference(self::CAT1_TICKET_REFERENCE, $cat1);
        $this->addReference(self::CAT2_TICKET_REFERENCE, $cat2);
        $this->addReference(self::CAT3_TICKET_REFERENCE, $cat3);


        //Types de méthodes d'obtention d'un ticket
        $concertTicket = 'ObtainingMethodCategory';

        $obtainingMethod1 = new RefValue();
        $obtainingMethod1->setCode($concertTicket . 'Guichet');
        $obtainingMethod1->setTypeCode($concertTicket . 'Code');
        $obtainingMethod1->setValue('Retrait au guichet');
        $manager->persist($obtainingMethod1);

        $obtainingMethod2 = new RefValue();
        $obtainingMethod2->setCode($concertTicket . 'ETicket');
        $obtainingMethod2->setTypeCode($concertTicket . 'Code');
        $obtainingMethod2->setValue('E-Ticket');
        $manager->persist($obtainingMethod2);

        $obtainingMethod3 = new RefValue();
        $obtainingMethod3->setCode($concertTicket . 'Envoi');
        $obtainingMethod3->setTypeCode($concertTicket . 'Code');
        $obtainingMethod3->setValue('Envoi postal');
        $manager->persist($obtainingMethod3);

        $manager->flush();

        $this->addReference(self::OBTAINING_METHOD1_REFERENCE, $obtainingMethod1);
        $this->addReference(self::OBTAINING_METHOD2_REFERENCE, $obtainingMethod2);
        $this->addReference(self::OBTAINING_METHOD3_REFERENCE, $obtainingMethod3);
    }
}
