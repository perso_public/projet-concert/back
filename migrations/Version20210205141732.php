<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210205141732 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, building VARCHAR(255) DEFAULT NULL, named_place VARCHAR(255) DEFAULT NULL, postal_code VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, line1 VARCHAR(255) NOT NULL, line2 VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_file (id INT AUTO_INCREMENT NOT NULL, filename VARCHAR(255) NOT NULL, size INT DEFAULT NULL, mime_type VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, category_id INT NOT NULL, image VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_23A0E66F675F31B (author_id), INDEX IDX_23A0E6612469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_liked (id INT AUTO_INCREMENT NOT NULL, article_id INT NOT NULL, user_id INT NOT NULL, date DATETIME NOT NULL, INDEX IDX_BDAEB9AB7294869C (article_id), INDEX IDX_BDAEB9ABA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_shared (id INT AUTO_INCREMENT NOT NULL, article_id INT NOT NULL, user_id INT NOT NULL, date DATETIME NOT NULL, INDEX IDX_794B633B7294869C (article_id), INDEX IDX_794B633BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE concert (id INT AUTO_INCREMENT NOT NULL, music_group_id INT NOT NULL, music_category_id INT DEFAULT NULL, concert_hall_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, image VARCHAR(255) NOT NULL, date DATETIME NOT NULL, opening_hours DATETIME NOT NULL, is_deleted TINYINT(1) NOT NULL, min_price DOUBLE PRECISION DEFAULT NULL, max_price DOUBLE PRECISION DEFAULT NULL, decreasing_percentage INT DEFAULT NULL, INDEX IDX_D57C02D239478561 (music_group_id), INDEX IDX_D57C02D2BC36F4F1 (music_category_id), INDEX IDX_D57C02D2C8B57370 (concert_hall_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE concert_hall (id INT AUTO_INCREMENT NOT NULL, address_id INT NOT NULL, location_id INT NOT NULL, name VARCHAR(255) NOT NULL, image VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_BE329CF8F5B7AF75 (address_id), INDEX IDX_BE329CF864D218E (location_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE concert_hall_reservation (id INT AUTO_INCREMENT NOT NULL, concert_hall_id INT NOT NULL, state_id INT NOT NULL, user_id INT NOT NULL, email VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, company VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) NOT NULL, date DATETIME NOT NULL, people_number INT NOT NULL, budget VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_615949D8C8B57370 (concert_hall_id), INDEX IDX_615949D85D83CC1 (state_id), INDEX IDX_615949D8A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE concert_reservation (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, email VARCHAR(255) NOT NULL, gender VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, line1 VARCHAR(255) NOT NULL, building VARCHAR(255) NOT NULL, named_place VARCHAR(255) DEFAULT NULL, postal_code VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, birthday DATE NOT NULL, purchase_date DATETIME NOT NULL, reservation_number VARCHAR(255) NOT NULL, update_date DATETIME DEFAULT NULL, INDEX IDX_467B8879A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE concert_ticket (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, concert_id INT NOT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_32AFBD2212469DE2 (category_id), INDEX IDX_32AFBD2283C97B2E (concert_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE concert_ticket_reservation (id INT AUTO_INCREMENT NOT NULL, concert_ticket_id INT NOT NULL, obtaining_method_id INT NOT NULL, concert_reservation_id INT DEFAULT NULL, insurance TINYINT(1) NOT NULL, span_number INT NOT NULL, row_number VARCHAR(255) NOT NULL, price DOUBLE PRECISION DEFAULT NULL, INDEX IDX_24F434404A8A3900 (concert_ticket_id), INDEX IDX_24F4344065EF07D6 (obtaining_method_id), INDEX IDX_24F434409EC2C5E (concert_reservation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, date DATETIME NOT NULL, message LONGTEXT NOT NULL, INDEX IDX_4C62E638A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE music_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, image VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, player_audio VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parking (id INT AUTO_INCREMENT NOT NULL, concert_hall_id INT NOT NULL, capacity INT NOT NULL, UNIQUE INDEX UNIQ_B237527AC8B57370 (concert_hall_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parking_reservation (id INT AUTO_INCREMENT NOT NULL, parking_id INT NOT NULL, concert_reservation_id INT NOT NULL, concert_id INT NOT NULL, user_id INT NOT NULL, email VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, spot_number INT NOT NULL, phone VARCHAR(255) NOT NULL, message LONGTEXT DEFAULT NULL, date DATETIME NOT NULL, INDEX IDX_1E678781F17B2DD (parking_id), INDEX IDX_1E6787819EC2C5E (concert_reservation_id), INDEX IDX_1E67878183C97B2E (concert_id), INDEX IDX_1E678781A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ref_value (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, type_code VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE restaurant (id INT AUTO_INCREMENT NOT NULL, concert_hall_id INT NOT NULL, menu VARCHAR(255) NOT NULL, capacity INT NOT NULL, UNIQUE INDEX UNIQ_EB95123FC8B57370 (concert_hall_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE restaurant_reservation (id INT AUTO_INCREMENT NOT NULL, restaurant_id INT NOT NULL, concert_reservation_id INT NOT NULL, concert_id INT NOT NULL, user_id INT NOT NULL, hour TIME DEFAULT NULL, email VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, people_number INT NOT NULL, phone VARCHAR(255) NOT NULL, message LONGTEXT DEFAULT NULL, date DATETIME NOT NULL, INDEX IDX_13B3734FB1E7706E (restaurant_id), INDEX IDX_13B3734F9EC2C5E (concert_reservation_id), INDEX IDX_13B3734F83C97B2E (concert_id), INDEX IDX_13B3734FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_user (role_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_332CA4DDD60322AC (role_id), INDEX IDX_332CA4DDA76ED395 (user_id), PRIMARY KEY(role_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, address_id INT NOT NULL, email VARCHAR(180) NOT NULL, password VARCHAR(255) NOT NULL, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(60) NOT NULL, phone VARCHAR(20) NOT NULL, register_date DATETIME NOT NULL, gender VARCHAR(30) NOT NULL, birthdate DATE NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D649F5B7AF75 (address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E6612469DE2 FOREIGN KEY (category_id) REFERENCES ref_value (id)');
        $this->addSql('ALTER TABLE article_liked ADD CONSTRAINT FK_BDAEB9AB7294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE article_liked ADD CONSTRAINT FK_BDAEB9ABA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE article_shared ADD CONSTRAINT FK_794B633B7294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE article_shared ADD CONSTRAINT FK_794B633BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE concert ADD CONSTRAINT FK_D57C02D239478561 FOREIGN KEY (music_group_id) REFERENCES music_group (id)');
        $this->addSql('ALTER TABLE concert ADD CONSTRAINT FK_D57C02D2BC36F4F1 FOREIGN KEY (music_category_id) REFERENCES ref_value (id)');
        $this->addSql('ALTER TABLE concert ADD CONSTRAINT FK_D57C02D2C8B57370 FOREIGN KEY (concert_hall_id) REFERENCES concert_hall (id)');
        $this->addSql('ALTER TABLE concert_hall ADD CONSTRAINT FK_BE329CF8F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE concert_hall ADD CONSTRAINT FK_BE329CF864D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE concert_hall_reservation ADD CONSTRAINT FK_615949D8C8B57370 FOREIGN KEY (concert_hall_id) REFERENCES concert_hall (id)');
        $this->addSql('ALTER TABLE concert_hall_reservation ADD CONSTRAINT FK_615949D85D83CC1 FOREIGN KEY (state_id) REFERENCES ref_value (id)');
        $this->addSql('ALTER TABLE concert_hall_reservation ADD CONSTRAINT FK_615949D8A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE concert_reservation ADD CONSTRAINT FK_467B8879A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE concert_ticket ADD CONSTRAINT FK_32AFBD2212469DE2 FOREIGN KEY (category_id) REFERENCES ref_value (id)');
        $this->addSql('ALTER TABLE concert_ticket ADD CONSTRAINT FK_32AFBD2283C97B2E FOREIGN KEY (concert_id) REFERENCES concert (id)');
        $this->addSql('ALTER TABLE concert_ticket_reservation ADD CONSTRAINT FK_24F434404A8A3900 FOREIGN KEY (concert_ticket_id) REFERENCES concert_ticket (id)');
        $this->addSql('ALTER TABLE concert_ticket_reservation ADD CONSTRAINT FK_24F4344065EF07D6 FOREIGN KEY (obtaining_method_id) REFERENCES ref_value (id)');
        $this->addSql('ALTER TABLE concert_ticket_reservation ADD CONSTRAINT FK_24F434409EC2C5E FOREIGN KEY (concert_reservation_id) REFERENCES concert_reservation (id)');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE parking ADD CONSTRAINT FK_B237527AC8B57370 FOREIGN KEY (concert_hall_id) REFERENCES concert_hall (id)');
        $this->addSql('ALTER TABLE parking_reservation ADD CONSTRAINT FK_1E678781F17B2DD FOREIGN KEY (parking_id) REFERENCES parking (id)');
        $this->addSql('ALTER TABLE parking_reservation ADD CONSTRAINT FK_1E6787819EC2C5E FOREIGN KEY (concert_reservation_id) REFERENCES concert_reservation (id)');
        $this->addSql('ALTER TABLE parking_reservation ADD CONSTRAINT FK_1E67878183C97B2E FOREIGN KEY (concert_id) REFERENCES concert (id)');
        $this->addSql('ALTER TABLE parking_reservation ADD CONSTRAINT FK_1E678781A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE restaurant ADD CONSTRAINT FK_EB95123FC8B57370 FOREIGN KEY (concert_hall_id) REFERENCES concert_hall (id)');
        $this->addSql('ALTER TABLE restaurant_reservation ADD CONSTRAINT FK_13B3734FB1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id)');
        $this->addSql('ALTER TABLE restaurant_reservation ADD CONSTRAINT FK_13B3734F9EC2C5E FOREIGN KEY (concert_reservation_id) REFERENCES concert_reservation (id)');
        $this->addSql('ALTER TABLE restaurant_reservation ADD CONSTRAINT FK_13B3734F83C97B2E FOREIGN KEY (concert_id) REFERENCES concert (id)');
        $this->addSql('ALTER TABLE restaurant_reservation ADD CONSTRAINT FK_13B3734FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE role_user ADD CONSTRAINT FK_332CA4DDD60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_user ADD CONSTRAINT FK_332CA4DDA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE concert_hall DROP FOREIGN KEY FK_BE329CF8F5B7AF75');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649F5B7AF75');
        $this->addSql('ALTER TABLE article_liked DROP FOREIGN KEY FK_BDAEB9AB7294869C');
        $this->addSql('ALTER TABLE article_shared DROP FOREIGN KEY FK_794B633B7294869C');
        $this->addSql('ALTER TABLE concert_ticket DROP FOREIGN KEY FK_32AFBD2283C97B2E');
        $this->addSql('ALTER TABLE parking_reservation DROP FOREIGN KEY FK_1E67878183C97B2E');
        $this->addSql('ALTER TABLE restaurant_reservation DROP FOREIGN KEY FK_13B3734F83C97B2E');
        $this->addSql('ALTER TABLE concert DROP FOREIGN KEY FK_D57C02D2C8B57370');
        $this->addSql('ALTER TABLE concert_hall_reservation DROP FOREIGN KEY FK_615949D8C8B57370');
        $this->addSql('ALTER TABLE parking DROP FOREIGN KEY FK_B237527AC8B57370');
        $this->addSql('ALTER TABLE restaurant DROP FOREIGN KEY FK_EB95123FC8B57370');
        $this->addSql('ALTER TABLE concert_ticket_reservation DROP FOREIGN KEY FK_24F434409EC2C5E');
        $this->addSql('ALTER TABLE parking_reservation DROP FOREIGN KEY FK_1E6787819EC2C5E');
        $this->addSql('ALTER TABLE restaurant_reservation DROP FOREIGN KEY FK_13B3734F9EC2C5E');
        $this->addSql('ALTER TABLE concert_ticket_reservation DROP FOREIGN KEY FK_24F434404A8A3900');
        $this->addSql('ALTER TABLE concert_hall DROP FOREIGN KEY FK_BE329CF864D218E');
        $this->addSql('ALTER TABLE concert DROP FOREIGN KEY FK_D57C02D239478561');
        $this->addSql('ALTER TABLE parking_reservation DROP FOREIGN KEY FK_1E678781F17B2DD');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E6612469DE2');
        $this->addSql('ALTER TABLE concert DROP FOREIGN KEY FK_D57C02D2BC36F4F1');
        $this->addSql('ALTER TABLE concert_hall_reservation DROP FOREIGN KEY FK_615949D85D83CC1');
        $this->addSql('ALTER TABLE concert_ticket DROP FOREIGN KEY FK_32AFBD2212469DE2');
        $this->addSql('ALTER TABLE concert_ticket_reservation DROP FOREIGN KEY FK_24F4344065EF07D6');
        $this->addSql('ALTER TABLE restaurant_reservation DROP FOREIGN KEY FK_13B3734FB1E7706E');
        $this->addSql('ALTER TABLE role_user DROP FOREIGN KEY FK_332CA4DDD60322AC');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66F675F31B');
        $this->addSql('ALTER TABLE article_liked DROP FOREIGN KEY FK_BDAEB9ABA76ED395');
        $this->addSql('ALTER TABLE article_shared DROP FOREIGN KEY FK_794B633BA76ED395');
        $this->addSql('ALTER TABLE concert_hall_reservation DROP FOREIGN KEY FK_615949D8A76ED395');
        $this->addSql('ALTER TABLE concert_reservation DROP FOREIGN KEY FK_467B8879A76ED395');
        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E638A76ED395');
        $this->addSql('ALTER TABLE parking_reservation DROP FOREIGN KEY FK_1E678781A76ED395');
        $this->addSql('ALTER TABLE restaurant_reservation DROP FOREIGN KEY FK_13B3734FA76ED395');
        $this->addSql('ALTER TABLE role_user DROP FOREIGN KEY FK_332CA4DDA76ED395');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE app_file');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE article_liked');
        $this->addSql('DROP TABLE article_shared');
        $this->addSql('DROP TABLE concert');
        $this->addSql('DROP TABLE concert_hall');
        $this->addSql('DROP TABLE concert_hall_reservation');
        $this->addSql('DROP TABLE concert_reservation');
        $this->addSql('DROP TABLE concert_ticket');
        $this->addSql('DROP TABLE concert_ticket_reservation');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE music_group');
        $this->addSql('DROP TABLE parking');
        $this->addSql('DROP TABLE parking_reservation');
        $this->addSql('DROP TABLE ref_value');
        $this->addSql('DROP TABLE restaurant');
        $this->addSql('DROP TABLE restaurant_reservation');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE role_user');
        $this->addSql('DROP TABLE user');
    }
}
